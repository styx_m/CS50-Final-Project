<?php


error_reporting(E_ALL ^ E_STRICT);
ini_set('display_errors', E_ALL ^ E_NOTICE);

require_once("../controller/helper.php");






  if ($_SERVER["REQUEST_METHOD"] == "GET") {

        // meta description used to show relevant info in navbar using js. Depends on word "super"
        $desc = (authorized(['super'])) ? 'View all scouts requirements (super)' : 'Info about requirements';

        render("pastActions.php", "History", $desc);

  }



  // if rank is changed, if regular user, display your specific requirements, if superuser, display all people with that rank or display info for rank of user selected
  if (isset($_POST['rank'])) {

      // security
      $rank = (string) test_input($_POST['rank']);
      $person = (string) test_input($_POST['individual']);




      if (!authorized(["super"])) {

          echo history($rank, $_SESSION['id']);

      }

      // if superuser, can look at anyone's history
      // change people to select if rank changes
      if ($_POST['rankChange'] == 'true') {

          echo superSelect($rank);

      }
  }

  // show the requirements of the rank selected for the person selected
  else if (isset($_POST['indRank'])) {

    $indRank = (string) test_input($_POST['indRank']);
    $person = (string) test_input($_POST['individual']);

    echo history($indRank, $person);
  }






  // if form is submitted, validate, and then if requirement was previously not filled in, fill in, else delete
  else if (isset($_POST['submit']) && authorized(["super"])) {


    // validation
    if (!isset($_POST['individuals']) || sizeof($_POST) <= 3 || !verifyToken("superHistory")) {
        redirect("history.php?status=failure");
        exit;
    }

    // cleaner design not to give option for date on this page, less cluttered
    $date = date("m/d/Y");


    // checkbox is the requirement _id
    if (isset($_POST['checkbox'])) {


        foreach($_POST['checkbox'] as $box) {

          $box = test_input($box);

          $req = dbAction($requirements, "findOne", "_id", new MongoDB\BSON\ObjectID($box));

          $check = dbAction($userReqs, "findOne", "users_id", new MongoDB\BSON\ObjectID(test_input($_POST['individuals'])));


            // if requirement was not filled, fill it
            if (!isset($check[$req['rank'] . $req['name']])) {

                $userReqs->updateOne(
                    ['users_id' => new MongoDB\BSON\ObjectID((string)$_POST['individuals'])],
                    ['$set' => [
                        $req['rank'] . $req['name'] =>
                            array('name' => $req['name'], 'date' => $date, 'signature' => new MongoDB\BSON\ObjectID($_SESSION['id']), 'req_id' => $req['_id'])
                                ]
                    ],

                    ['upsert' => true]

                );
            }

            // if it was filled, delete it
            else {

                $userReqs->updateOne(
                    ['users_id' => new MongoDB\BSON\ObjectID((string) test_input($_POST['individuals']))],
                    ['$unset' =>
                        [$req['rank'] . $req['name'] => ""]
                    ]

                );

            }

        }


    }

    // finds all requirements that are not checkboxes (are stored in db as 1a1 etc)
    $oddReqs = $requirements->find(
        ['name' => ['$regex' => '^[0-9]{1}[a-z]{1}[0-9]{1}$']]
    );


    // max loop ~8
    foreach($oddReqs as $oddReq) {

        $textReq = new MongoDB\BSON\ObjectID($oddReq['_id']);

        if (!isset($_POST[(string) $textReq]) || $_POST[(string) $textReq] !== "0") {
            // without this, no updates to non-checkboxes work. By mistake deleted before presentation since thought didn't need, so glided over the bug
            if (empty($_POST[ (string) $textReq])) {

                continue;
          }

        }


        // make sure the improvement is improved
        if (strtok($oddReq['label'], " ") == "Improvement") {


            $word = explode(" ", $oddReq['label']);

            $new = str_replace('c', 'a', $oddReq['name']);

            $valid = dbAction($userReqs, "findOne", "users_id", new MongoDB\BSON\ObjectID($_POST['individuals']), "_id");



            if (!isset($valid[$oddReq['rank'] . $new]['value'])) {

                redirect("/history.php?status=failure");
                exit;
            }


            $firstAttempt = $valid[$oddReq['rank'] . $new]['value'];



            // time should be less
            if ($word[sizeof($word) - 1] == "walk/run.") {


                // if improvement is not improved (i.e if it took longer)
                if (date($firstAttempt) <= date($_POST[(string) $textReq])) {
                    redirect("/history.php?status=failure");
                    exit;
                }

            }

            else {
                // anything besides time should be more
                if ($firstAttempt >= $_POST[(string) $textReq]) {
                    redirect("/history.php?status=failure");
                    exit;
                }

            }

        }



        $userReqs->updateOne(
            ['users_id' => new MongoDB\BSON\ObjectID(test_input($_POST['individuals']))],
            ['$set' => [
                $oddReq['rank'] . (string)$oddReq['name'] =>
                    ['name' => $oddReq['name'], 'value' => test_input($_POST[(string) $textReq]), 'date' => $date, 'signature' => new MongoDB\BSON\ObjectID($_SESSION['id']), 'req_id' => $oddReq['_id']]
                        ]
            ],

            ['upsert' => true]

        );

    }


      redirect("history.php?status=success");

  }



?>
