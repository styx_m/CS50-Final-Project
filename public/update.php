<?php



require_once("../controller/helper.php");





if ($_SERVER["REQUEST_METHOD"] == "GET") {

    render("index.php", "Ranks", "List of scouts, by rank (super)");
}


// if rank is changed, find all requirements of that rank and display as radio buttons
else if (isset($_POST["rank"])) {

    $info = dbAction($requirements, "find", "rank", test_input($_POST["rank"]), "_id");

    echo massFill($info, "reqs", "label", "_id", "radio", "checked='checked'");

    // to prevent xxs attacks
    $formToken = generateToken('bulkUpdate');

    echo "<input type='hidden' name='token' value='{$formToken}' />";



}

// if requirement is changed, fill in the users who don't have it. Function defined at bottom of page
else if (isset($_POST["req"])) {

    fillUsers($_POST["req"]);
}


// if sorting method is changed, sort differently
else if (isset($_POST["sort"])) {

    fillUsers($_POST["reqSort"], (string) test_input($_POST["sort"]));

}


// if form submitted, validate and then put info into database. If user never had an entry in userReqs, add to collection
else if (isset($_POST["submitSignOff"])) {

      // validation
      if (!isset($_POST["scout"]) || !isset($_POST["reqs"]) || !authorized(["super"]) || !verifyToken("bulkUpdate")) {
          redirect("update.php?status=failure");
          exit;
      }



    if ($_POST["date"] != "") {

        if (!preg_match("/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/", $_POST["date"], $matches) || $matches[0] > date("m/d/Y")) {
            redirect("update.php?status=failure");
            exit;
        }

        $date = test_input($_POST["date"]);
    }

    else {
      $date = date("m/d/Y");
    }



    $req = dbAction($requirements, "findOne", "_id", new MongoDB\BSON\ObjectID((string) test_input($_POST["reqs"])));


    $mongoPerson = [];


    // this loop could (should?) be avoided by automatically adding users_id to userReqs upon creating an account, however don't know what Mr Reichner wants so will leave as is. If delete, leave mongoPerson
    foreach($_POST["scout"] as $person) {

      $person = (string) test_input($person);

        $users_id = $userReqs->findOne(
            ["users_id" => new MongoDB\BSON\ObjectID($person)],
            ["projection" => ["_id" => 0, "users_id" => 1]]

        );

        if (!isset($users_id)) {

          dbAction($userReqs, "InsertOne", "users_id", new MongoDB\BSON\ObjectID($person));

        }

        $mongoPerson[] = new MongoDB\BSON\ObjectID($person);

    }



    $updates = $userReqs->updateMany(
        ["users_id" => ['$in' => $mongoPerson]],
        ['$set' => [
            (string) $req['rank'] . $req["name"] =>
                ["name" => $req["name"], "date" => $date, "signature" => new MongoDB\BSON\ObjectID($_SESSION["id"]), 'req_id' => (string) $req["_id"]]
                    ]
        ]

    );

    if (count($updates) < 1) {
        redirect("update.php?status=failure");
    }

    else {

    redirect("update.php?status=success");
    }
}








    // find all users who don't have requirement and sort in manner specified
    function fillUsers($reqPost, $sortKey = "lastName", $sortOrder = 1) {

      global $userReqs, $users, $requirements;


      $info = dbAction($requirements, "findOne", "_id", new MongoDB\BSON\ObjectID((string) test_input($reqPost)), "_id");


      // find all users who have the requirement filled out
      $checks = $userReqs->find(
          [$info['rank'] . $info['name'] => ['$exists' => 1]],
          ['projection' => ["_id" => 0, "users_id" => 1]]

      );


      $filled = [];

      // makes an array of all the people who have the requirement
      foreach ($checks as $check) {
          $filled[] = new MongoDB\BSON\ObjectID((string) test_input($check["users_id"]));
      }


      // finds all users who do NOT have the requirement
      $user = $users->find(

          ["_id" => ['$nin' => $filled]],
          ['sort' => [$sortKey => $sortOrder]]
      );


      $nameSort = ($sortKey === "firstName") ? "firstName lastName" : "lastName firstName";


      echo massFill($user, "scout[]", $nameSort, "_id");

    }

?>
