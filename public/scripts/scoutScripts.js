$(document).ready(function() {


  // if only hide on login and defualt show everywhere else, flicker
  if (window.location.pathname !== "/login.php") {
      $(".navbar").css("display", "block");
  }

  var desc = $("meta[name='description']").attr('content');
  var auth = false;



  // navbar depends on user
  if (/super/i.test(desc)) {
      $(".superNav").css("display", "block");

      auth = true;
  }

  else {

    $(".regNav").css("display", "block");
  }



    // place for alerts such as success/error messages
    $("h1").after("<div class='info'></div>");

    status();


    if (window.location.pathname == "/update.php") {

        updatePage();
    }


    // check comments on history.php for info
    else if (window.location.pathname == "/history.php") {

      // add content that only applies to superusers
      if (auth) {
          $("th:nth-child(4)").after("<th> Add/Remove </th>");
          $("table").after("<input type='submit' name='submit' id='singleSubmit' value='Submit' />");
          $(".hideReg").css("display", "block");
      }

        loadHistory(true);

        $("#historyRanks").change(function() {

            loadHistory(true);

        });


        $("#individuals, #indRanks").change(function() {

          loadHistory(false, true);

        });

    }

});






  function updatePage() {

    // to make code more understandable (and laziness), this variable will hold all key value pairs for ajax per call
    var values;


      // right away load all requirements and people for the first scout requirement
      values = { 'rank': $("#ranks").val() }
      populate(values, '#reqs');


      // on rank change load requirements of that rank
      $("#ranks").change(function() {

          values = { 'rank': $(this).val() }
          populate(values, '#reqs');

      });


      // when select requirement, load people who don't have it
      $("body").on("click", "input[name^='reqs']", function(){

          values = { "req" : $(this).val() }

          populate(values, '#people');
      });


      // sort people by select options
      $("#sortBy").change(function() {

          values = { 'sort' : $(this).val(), 'reqSort' : $("input[name='reqs']:checked").val()}

          populate(values, "#people");


      });


      // ajax calls for everything in update.php except submissions (which I think are more authentic when page reloads and slothful)
      function populate(values, locationId) {

          $.post({
              url: '../../update.php',
              type: 'POST',
              data: values,
              success: function(data, status) {


                  $(locationId).html(data);

                  // adds the people if just got to this page (and so didn't select a requirement yet)
                  if (typeof values.rank !== 'undefined') {

                    values = {'req' : $("input[name^='reqs']").val() }

                    populate(values, '#people');
                  }

              },

              error: function(xhr, desc, err) {

                  console.log(xhr);
                  console.log("Details: " + desc + "\nError:" + err);
              }

          });
      }





      // little question mark next to date marks it as optional with this giving more info
      $("#dateInfo").click(function() {

          $("#infoDiv").css("visibility", "visible");
      });



      // upon attempt to submit form, make sure everything is formatted correctly and that what needs to be filled in, is
      $("#submit").click(function() {


          // the :checkbox:checked class selects all checkboxes in the div and the :checked.length gives the number that is checked
          var scoutBoxes = $('#people input:checkbox:checked');
          var reqBoxes = $('#reqs input:radio:checked');
          var date = $("#date").val();

          var errorMessage = "";


          // if at least 1 scout and 1 requirement is checked, (and if there's a date, that it's formatted) submit, else, don't
          if (scoutBoxes.length == 0) {

              errorMessage += "You must select at least one person. <br class='line' />";
          }

          // this should never happen, as first radio is checked automatically, but just in case
          if (reqBoxes.length == 0) {

              errorMessage += "You must select a requirement. <br class='line'/>";
          }

          if (date != "" && !checkDate(date)) {

              errorMessage += "Please enter a correctly formatted date <br class='line' />";
          }


          if (errorMessage == "") {

              $("#reqForm").submit();

          } else {

              $(".info").html(errorMessage).addClass("text-danger bg-danger").css("display", "block");

              window.scrollTo(0, 0);
              return false;
          }


      });


      function checkDate(date) {

          var patt = new RegExp("^([0-9]{2})/([0-9]{2})/([0-9]{4})$");

          var dateSeg = date.split("/");

          if (dateSeg[0] > 12 || dateSeg[1] > 31 || dateSeg[2] > 2016) {
              return false;
          }


          return patt.test(date);
      }

  }


// loads history depending on paremeters given. See history.php for details
function loadHistory(rankStatus = false, reqRank = false) {

  // if changing individual's rank or not
  if (reqRank == true) {
      var data = {'indRank': $("#indRanks").val()}
  }

  else {
      var data = {'rank': $("#historyRanks").val(), 'rankChange' : rankStatus}
  }

  data['individual'] = $("#individuals").val();


  $.post({
      url: '../../history.php',
      type: 'POST',
      data: data,
      success: function(data, status) {


          // if rank (and so individual as well) changed in the superuser page
          if (reqRank == false && /End/.test(data)) {

              var data = data.split("End");

              $("#individuals").html(data[0]);
              $("#historyContent").html(data[1]);
              $("#indRanks").val($("#historyRanks").val());
          }

          else {

              $("#historyContent").html(data);
          }

          $(".completed").parent().addClass("success");


      },

      error: function(xhr, desc, err) {

          console.log(xhr);
          console.log("Details: " + desc + "\nError:" + err);
      }
  });
}


// gives success/error message passed from php validation
function status() {
    // if requirements were addedd successfully, user redirected to success message
    if (/\?status=success/.test(location)) {

        var message = "Successfully updated requirements.";

        $(".info").html(message).addClass("text-success bg-success").css("display", "block");

    }

    else if (/\?status=failure/.test(location)) {
        var message = "There was an error processing your request. Please make sure all fields have valid inputs.";

        $(".info").html(message).addClass("text-danger bg-danger").css("display", "block");

    }


}
