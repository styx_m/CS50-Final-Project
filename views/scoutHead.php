<!DOCTYPE html>
<html>
<head>
	<title>  <?= htmlspecialchars($title); ?> </title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?= htmlspecialchars($description); ?>" />

	<!-- using local versions just in case no wifi and want to work -->


			<link type="text/css" rel="stylesheet" href="./styles/bootstrap.min.css"/>
      <!-- font awesome -->
      <link type="text/css" rel="stylesheet" href="./styles/font-awesome.min.css" />
			<script type="text/javascript" src="./scripts/jquery-3.1.0.js"></script>
			<script type="text/javascript" src="./scripts/bootstrap.min.js"></script>


			<script type="text/javascript" src="./scripts/scoutScripts.js"></script>

      <link type="text/css" rel="stylesheet" href="./styles/scoutStyles.css"/>

</head>
<body>


	<!-- makes the header black and position fixed -->
	<div class="navbar navbar-inverse navbar-fixed-top">

	  <div class="container-fluid">
	    <!--makes the header -->

	    <div class="navbar-header">



	      <!--makes the menu button (visible if on small screen) -->
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	       <!--screen reader friendly -->
	        <span class="sr-only">
	          Toggle Navigation </span>
	        <!-- bars that make up the menu button -->
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>

	    </div>

	    <!-- once screen reaches certain width, navbar collapses -->
	    <div class="collapse navbar-collapse">

	      <ul class="nav navbar-nav" id="navNames">


						<li class="superNav"> <a href='/update.php'> Bulk Update </a> </li>
						<li> <a href='/history.php'> History </a> </li>
						<li> <a href='/logout.php'> Logout </a> </li>





	      </ul>

	    </div>

	  </div>

	</div>
