<?php

  // take off sometime or other, leaving all error stuff on (at least) until Mr Reichner looks at it
  error_reporting(E_ALL ^ E_STRICT);
  ini_set('display_errors', E_ALL ^ E_NOTICE);

  require_once("config.php");



  date_default_timezone_set('EST');



  // renders page from head and foot templates
  function render($page, $title = "Scout Troop 37", $description = "Website for Scout Troop 37") {

    require_once("../views/scoutHead.php");
    require_once("../views/{$page}");
    require_once("../views/foot.php");

  }



  // redirects user
  function redirect($location) {
        if (headers_sent($file, $line))
        {
            trigger_error("HTTP headers already sent at {$file}:{$line}", E_USER_ERROR);
        }

        header("Location: {$location}");

        exit;
  }




  // does any non-specific action involving database
  function dbAction($collection, $query, $key, $field, $sortBy = '_id') {


      if ($key != "") {

          $persons = $collection->$query([$key => $field],
          ['sort' => ['lastName' => 1]]

          );
      }


      if ($persons) {
          return $persons;
      }

      return false;
  }



  define('scoutsPerCol', 10);

  // creates almost all inputs in update.php
  function massFill($queryResults, $name, $labels, $vals = "on", $type = "checkbox", $pref="", $class = "checkbox") {

    // seems cleaner to save as variable rather than echo straight away
    $result = "";

    $count = 0;
    $len = count($queryResults);


     foreach ($queryResults as $queryResult) {


          $val = $queryResult[$vals];


          // if name is scout[], two inputs are in the function call (first and last name), so this separates them, and depending on order, adds a comma
          if ($name == "scout[]") {

              $labelsHolder = explode(" ", $labels);

              if ($labelsHolder[0] == 'lastName') {
                  $label = $queryResult[$labelsHolder[0]] . ", " . $queryResult[$labelsHolder[1]];
              }

              else {
                $label = $queryResult[$labelsHolder[0]] . " " . $queryResult[$labelsHolder[1]];

              }

              // starts a column
              if ($count % scoutsPerCol == 0) {

                  $result .= "<div class='col-lg-1 col-md-2 col-sm-2 col-xs-4 scoutCols'>";
              }

          }

          else {

            $label = $queryResult[$labels];

            // these keywords signify that the requirement is user-specific, therefore don't list in bulk update
            if (strtok($label, " ") == "Record" || strtok($label, " ") == "Improvement") {
                  continue;
            }

            // adds some formatting
            if ($type == "radio") {
              $label = "&nbsp;&nbsp;" . $label;

            }

            // in this case, $pref is attr checked, and only want first radio to be that way
            if ($count == 1) {
                $pref = "";
            }

          }


        // actual input part
        $result .= "<div class='form-group'>

                  <div class='{$class}'>
                    <label>" .



                      "<input value='{$val}' name='{$name}' $pref type='{$type}' />" .

                             $label .

                  "</label>
                      </div>
                  </div>";




            $count++;

            if ($name == "scout[]") {

                // ends column
                if ($count % scoutsPerCol == 0) {
                    $result .= "</div>";
                }

            }

      }

    return $result;
  }







  function history($rank, $userId) {

      global $requirements, $users, $userReqs;


      // gets all requirements for scouts of $rank
      $reqs = dbAction($requirements, "find", "rank", $rank);

      // gets users_id whose info is being filled
      $userFilled = dbAction($userReqs, 'findOne', 'users_id', new MongoDB\BSON\ObjectID($userId));

      $auth = authorized(["super"]);

      $result = "";

      foreach($reqs as $req) {

          if (isset($userFilled[$req['rank'] . $req['name']]['value'])) {

              $filled = false;



              echo $userFilled[$req['rank'] . $req['name']]['value'];

              if (preg_match('/a4$/', $userFilled[$req['rank'] . $req['name']]['name']) === 1) {
                                // if time is not all zeros
                  if (preg_match('/[1-9]/', $userFilled[$req['rank'] . $req['name']]['value'])) {
                      $filled = true;

                  }

              }
              // if anything besides time is not zero
              else if ($userFilled[$req['rank'] . $req['name']]['value'] > 0) {
                  $filled = true;
              }

          }

          $result .= "<tr>
                    <td>" . $req['label'] . "</td>";



          // if requirement is filled out, mark as such with proper info
          if (isset($userFilled[$req['rank'] . $req['name']]) && !isset($userFilled[$req['rank'] . $req['name']]['value']) || isset($userFilled[$req['rank'] . $req['name']]) && $filled === true) {

              $result .= "<td class='completed'>";

              // if input is not a checkbox, but rather a string/number such as pushups
              if (isset($userFilled[$req['rank'] . $req['name']]['value'])) {

                  $result .=  $userFilled[$req['rank'] . $req['name']]['value'];
              }

              else {
                  $result .= "Completed";
              }


              $result .= "</td>
                          <td>" . $userFilled[$req['rank'] . $req['name']]['date'] . "</td>";



              $signature = dbAction($users, "findOne", "_id", $userFilled[$req['rank'] . $req['name']]['signature']);

              $result .= "<td>" . $signature['firstName'] . " " . $signature['lastName'] . "</td>";

          }


          else {
              $result .= "<td>To Do </td>
                          <td></td>
                          <td></td>";
          }

          if ($auth) {
              // label so can just click anywhere in cell to check box
              $result .= "<label><td>" . historyInput($req['label'], $req['_id']) . "</td></label>";
          }

          $result .= "</tr>";

      }

      $formToken = generateToken('superHistory');
      $result .= "<input type='hidden' name='token' value='{$formToken}' />";

      return $result;
  }





    // creates inputs in history.php (superuser version)
    function historyInput($label, $req_id) {
        // keywords for non checkboxes
        if (strtok($label, " ") == "Record" || strtok($label, " ") == "Improvement") {


            if (strpos($label, "walk")) {

                  return "<input type='text' pattern='[0-9]{2}:[0-9]{2}' placeholder='01:10' name='{$req_id}' />";

            }

            else if (strpos($label, "cm")) {

                  return "<input type='number' pattern='[0-9]{1,2}' name='{$req_id}' />";
            }


            else {
                // magic number 150 pushups in 1 minute. World record is 138
                return "<input type='number' min='0' max='150' name='{$req_id}' />";
            }

        }

        else {

            return "<input type='checkbox' name='checkbox[]' value='{$req_id}' />";
        }
    }


  // for history.php to generate users as select options (superuser)
  function superSelect($rank, $userId = "") {

      global $users;

      $rank = (string) $_POST['rank'];

      // find all users of $rank and display as options in select
      $persons = dbAction($users, "find", "rank", $rank);



      foreach ($persons as $person) {

        // $userId is the first person in select. Info displayed automatically
        if ($userId == "") {
            $userId = $person['_id'];
        }


        echo  "<option value='{$person['_id']}'>" . $person['lastName'] . ", " . $person['firstName'] . "</option>";
      }


      echo "End";

       echo history($rank, $userId);


  }


/********************
*     Security      *
*********************/


  // checks if user is authorized to be on certain pages
  // making $type an array in preperation for super-duper user that Mr Reichner said he'll be
  function authorized($type = []) {

      global $users;

      if (empty($_SESSION['id'])) {
          return false;
      }

      $authority = dbAction($users, "findOne", "_id", new MongoDB\BSON\ObjectID($_SESSION["id"]));


      if (in_array($authority['type'], $type)) {

        return true;
      }

      return false;
  }




  // security for post/get requests http://www.w3schools.com/php/php_form_validation.asp
  function test_input($data) {

      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }




  // when log in use this for added security against cross site scripting
  // https://css-tricks.com/serious-form-security/
  function generateToken($value) {

    $token = password_hash($value, PASSWORD_DEFAULT);

    $_SESSION[$value . 'token'] = $token;

    return $token;

  }

  function verifyToken($value) {

      if (!isset($_SESSION[$value . 'token'])) {
          return false;
      }

      if (!isset($_POST['token'])) {
          return false;
      }

      if ($_SESSION[$value . 'token'] != $_POST['token']) {
        return false;
      }

      return true;
  }



  // puts a red asterisk by a required field
function req() {
    echo '<span class="star">*</span>';
}

?>
